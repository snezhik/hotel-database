#ifndef HOTELWINDOW_H
#define HOTELWINDOW_H

#include "hoteldb.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class HotelWindow; }
QT_END_NAMESPACE

class QPrinter;
class QTableView;

class HotelWindow : public QMainWindow
{
    Q_OBJECT

public:
    HotelWindow(QWidget *parent = nullptr);
    ~HotelWindow();

private:
    void initializeTables();
    void initializeTableView(QTableView* table);
    void initializePermissions();
    int getSelectedRow(const QTableView* table);

    void updateGuestsTable(const HotelDB::guests_filter_t* filter);

    Ui::HotelWindow *ui;
    HotelDB::HotelDB m_db;
    int m_user_group{-1};
    HotelDB::guests_filter_t m_guests_filter{};

public slots:
    void on_login_pb_clicked();

    void on_guests_search_pb_clicked();
    void on_guests_reset_pb_clicked();
    void on_guests_add_pb_clicked();
    void on_guests_view_customContextMenuRequested(const QPoint& pos);
    void on_guests_view_removeGuest();
    void on_guests_view_findCleaner();
    void on_guests_view_printBill();
    void on_print_bill_paintRequest(QPrinter* printer);

    void on_employee_add_pb_clicked();
    void on_employees_view_customContextMenuRequested(const QPoint& pos);
    void on_employees_view_removeEmployee();
};
#endif // HOTELWINDOW_H
