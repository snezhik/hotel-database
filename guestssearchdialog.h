#ifndef GUESTSSEARCHDIALOG_H
#define GUESTSSEARCHDIALOG_H

#include <QDialog>

#include "hoteldb.h"

namespace Ui {
class GuestsDialog;
}

class QLineEdit;

class GuestsSearchDialog : public QDialog
{
    Q_OBJECT

public:
    GuestsSearchDialog(HotelDB::guests_filter_t* guests_filter,
                       QWidget *parent = nullptr);
    ~GuestsSearchDialog();

private:
    void setLETextFromStr(QLineEdit* le, QString text,
                          QString invalid_value = SQL_INVALID_TEXT);
    void setLETextFromInt(QLineEdit* le, int number, int invalid_value = SQL_INVALID_INT);

    template<typename ValueType>
    void setFilterValue(ValueType& filter, ValueType value, ValueType invalid_value);

    void initializeUI();

    Ui::GuestsDialog* ui{nullptr};
    HotelDB::guests_filter_t* m_guests_fiter{nullptr};

public slots:
    void on_action_pb_clicked();
};

#endif // GUESTSSEARCHDIALOG_H
