#include "hoteldb.h"

#include <QApplication>
#include <QCryptographicHash>
#include <QDebug>
#include <QDir>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>

#define DB_DRIVER "SQLITECIPHER"

namespace HotelDB {

class HotelDB::HotelDBImpl {
public:
    QSqlDatabase db;
    int user_group{-1};
    QSqlQueryModel guests_model;
    QSqlQueryModel employees_model;
    QSqlQueryModel rooms_model;
};

HotelDB::HotelDB(QObject *parent)
    : QObject(parent)
{}

HotelDB::~HotelDB() = default;

int HotelDB::open(QString db_path, QString login, QString password)
{
    if (!QSqlDatabase::drivers().contains(DB_DRIVER)) {
        qDebug() << "incorrect driver:" << DB_DRIVER;
        qDebug() << "supported drivers: " << QSqlDatabase::drivers();
        return -1;
    }

    db_path = QApplication::applicationDirPath() + QDir::separator() + db_path;

    qDebug() << "database name:" << db_path;

    m_pimpl = std::make_unique<HotelDBImpl>();
    m_pimpl->db = QSqlDatabase::addDatabase(DB_DRIVER);
    m_pimpl->db.setDatabaseName(db_path);

    if (!m_pimpl->db.open()) {
        qDebug("cannot open database: '%s' (%s)", qPrintable(db_path),
               qPrintable(m_pimpl->db.lastError().text()));
        close();
        return -1;
    }

    if (!validateUser(login, password)) {
        close();
        return -1;
    }

    return m_pimpl->user_group;
}

void HotelDB::close()
{
    m_pimpl.reset();
    QSqlDatabase::removeDatabase(QSqlDatabase::defaultConnection);
}

QAbstractItemModel* HotelDB::getGuestsModel()
{
    return updateGuestsModel(nullptr) ? &m_pimpl->guests_model : nullptr;
}

bool HotelDB::updateGuestsModel(const guests_filter_t* filter)
{
    QString query =
        "SELECT g.id,g.surname,g.firstname,g.patronym,g.passport,g.city,g.arrival_date,\n"
        "       g.duration,r.number\n"
        "FROM guests AS g\n"
        "  INNER JOIN rooms AS r\n"
        "  INNER JOIN guests_rooms as gr\n"
        "  ON r.id = gr.rooms_id\n"
        "  AND g.id = gr.guests_id\n";

    if (filter != nullptr) {
        QString where = "WHERE\n";

        if (filter->firstname != SQL_INVALID_TEXT) {
            query += where;
            query += QString("g.firstname LIKE '%1%'\n").arg(filter->firstname);
            where.clear();
        }

        if (filter->surname != SQL_INVALID_TEXT) {
            query += where.isEmpty() ? "AND\n" : where;
            query += QString("g.surname LIKE '%1%'\n").arg(filter->surname);
            where.clear();
        }

        if (filter->patronym != SQL_INVALID_TEXT) {
            query += where.isEmpty() ? "AND\n" : where;
            query += QString("g.patronym LIKE '%1%'\n").arg(filter->patronym);
            where.clear();
        }

        if (filter->passport != SQL_INVALID_TEXT) {
            query += where.isEmpty() ? "AND\n" : where;
            query += QString("g.passport LIKE '%1%'\n").arg(filter->passport);
            where.clear();
        }

        if (filter->city != SQL_INVALID_TEXT) {
            query += where.isEmpty() ? "AND\n" : where;
            query += QString("g.city LIKE '%1%'\n").arg(filter->city);
            where.clear();
        }

        if (filter->arrival_date != SQL_INVALID_TEXT) {
            query += where.isEmpty() ? "AND\n" : where;
            query += QString("g.arrival_date LIKE '%1%'\n").arg(filter->arrival_date);
            where.clear();
        }

        if (filter->duration != SQL_INVALID_INT) {
            query += where.isEmpty() ? "AND\n" : where;
            query += QString("g.duration = %1\n").arg(filter->duration);
            where.clear();
        }

        if (filter->room_number != SQL_INVALID_INT) {
            query += where.isNull() ? "AND\n" : where;
            query += QString("r.number = %1\n").arg(filter->room_number);
            where.clear();
        }
    }

    qDebug() << "query:" << query;
    m_pimpl->guests_model.setQuery(query);

    if (m_pimpl->guests_model.lastError().isValid()) {
        qDebug("failed to get guests table (%s)",
               qPrintable(m_pimpl->guests_model.lastError().text()));
        return false;
    }

    QStringList headers = {tr("ID"), tr("Фамилия"), tr("Имя"), tr("Отчество"),
                           tr("Номер паспорта"), tr("Город"), tr("Дата заселения"),
                           tr("Период проживания"), tr("Номер комнаты")};

    for (int i{0}; i < m_pimpl->guests_model.columnCount(); i++) {
        m_pimpl->guests_model.setHeaderData(i, Qt::Horizontal, headers[i]);
    }

    return true;
}

bool HotelDB::addGuest(const guests_filter_t& guest_values)
{
    int guest_id{-1};

    if (guest_id = getMaxID("guests"); guest_id == -1) {
        return false;
    }

    ++guest_id;
    qDebug() << "new guest id:" << guest_id;

    QSqlQuery query(m_pimpl->db);

    query.prepare(
        "INSERT INTO guests (id,firstname,surname,patronym,passport,city,arrival_date,"
        "                    duration)\n"
        "VALUES (?,?,?,?,?,?,?,?)");
    query.addBindValue(guest_id);
    query.addBindValue(guest_values.firstname);
    query.addBindValue(guest_values.surname);
    query.addBindValue(guest_values.patronym);
    query.addBindValue(guest_values.passport);
    query.addBindValue(guest_values.city);
    query.addBindValue(guest_values.arrival_date);
    query.addBindValue(guest_values.duration);

    if (!query.exec()) {
        qDebug("failed to add guest (%s)", qPrintable(query.lastError().text()));
        return false;
    }

    query.prepare("SELECT id FROM rooms WHERE number = ?");
    query.addBindValue(guest_values.room_number);

    if (!query.exec() || !query.next()) {
        qDebug("cannot get room id (%s)", qPrintable(query.lastError().text()));
        removeGuest(guest_id);
        return false;
    }

    int room_id = query.value(0).toInt();

    qDebug() << "room id:" << room_id;

    query.prepare("INSERT INTO guests_rooms (guests_id, rooms_id)\n"
                  "VALUES (?,?)");
    query.addBindValue(guest_id);
    query.addBindValue(room_id);

    if (!query.exec()) {
        qDebug("failed to bind guest and room (%s)",
               qPrintable(query.lastError().text()));
        removeGuest(guest_id);
        return false;
    }

    return true;
}

bool HotelDB::removeGuest(int id)
{
    return removeRecord("guests", "id", id) &&
           removeRecord("guests_rooms", "guests_id", id);
}

QString HotelDB::findCleaner(int guest_id, int weekday_id)
{
    QSqlQuery query(
        "SELECT e.surname, e.firstname, e.patronym FROM employees AS e\n"
        "  INNER JOIN guests as g ON g.id = ?\n"
        "  INNER JOIN rooms AS r\n"
        "  INNER JOIN guests_rooms AS gr\n"
        "  ON r.id = gr.rooms_id AND g.id = gr.guests_id\n"
        "  INNER JOIN timetable AS t\n"
        "  ON t.floor = r.floor AND t.employees_id = e.id\n"
        "  AND t.weekday_id = ?;");

    query.addBindValue(guest_id);
    query.addBindValue(weekday_id);

    if (!query.exec() || !query.next()) {
        qDebug("failed to get cleaner (%s)", qPrintable(query.lastError().text()));
        return QString();
    }

    QStringList cleaner;

    for (int i{0}; i < 3; i++) {
        cleaner << query.value(i).toString();
    }

    return cleaner.join(' ');
}

QAbstractItemModel* HotelDB::getEmployeesModel()
{
    if (m_pimpl->user_group == Groups::MANAGER) {
        return nullptr;
    }

    return updateEmployeesModel() ? &m_pimpl->employees_model : nullptr;
}

bool HotelDB::updateEmployeesModel()
{
    if (m_pimpl->user_group == Groups::MANAGER) {
        return false;
    }

    QString query =
        "SELECT e.id,e.surname,e.firstname,e.patronym,f.floor,w.description\n"
        "FROM employees AS e\n"
        "  INNER JOIN floors AS f\n"
        "  INNER JOIN weekday as w\n"
        "  INNER JOIN timetable AS t\n"
        "  ON f.floor = t.floor AND e.id = employees_id AND w.id = weekday_id";

    qDebug() << "query:" << query;
    m_pimpl->employees_model.setQuery(query);

    if (m_pimpl->employees_model.lastError().isValid()) {
        qDebug("failed to get guests table (%s)",
               qPrintable(m_pimpl->employees_model.lastError().text()));
        return false;
    }

    QStringList headers = {tr("ID"), tr("Фамилия"), tr("Имя"), tr("Отчество"),
                           tr("Этаж"), tr("День недели")};

    for (int i{0}; i < m_pimpl->employees_model.columnCount(); i++) {
        m_pimpl->employees_model.setHeaderData(i, Qt::Horizontal, headers[i]);
    }

    return true;
}

bool HotelDB::addEmployee(const employees_filter_t &employee)
{
    if (m_pimpl->user_group >= Groups::MANAGER) {
        return false;
    }

    int employee_id{-1};

    if (employee_id = getMaxID("employees"); employee_id == -1) {
        return false;
    }

    ++employee_id;

    QSqlQuery query(m_pimpl->db);

    query.prepare("INSERT INTO employees (id,firstname,surname,patronym)\n"
                  "VALUES (?,?,?,?)");

    query.addBindValue(employee_id);
    query.addBindValue(employee.firstname);
    query.addBindValue(employee.surname);
    query.addBindValue(employee.patronym);

    if (!query.exec()) {
        qDebug("failed to add employee (%s)", qPrintable(query.lastError().text()));
        return false;
    }

    query.prepare("INSERT INTO timetable (floor,employees_id,weekday_id)\n"
                  "VALUES (?,?,?)");

    query.addBindValue(employee.floor);
    query.addBindValue(employee_id);
    query.addBindValue(employee.weekday);

    if (!query.exec()) {
        qDebug("failed update timetable (%s)", qPrintable(query.lastError().text()));
        removeEmployee(employee_id);
        return false;
    }

    return true;
}

bool HotelDB::removeEmployee(int id)
{
    if (m_pimpl->user_group >= Groups::MANAGER) {
        return false;
    }

    return removeRecord("employees", "id", id) &&
           removeRecord("timetable", "employees_id", id);
}

QAbstractItemModel* HotelDB::getRoomsModel()
{
    if (m_pimpl->user_group >= Groups::EMPLOYEE) {
        return nullptr;
    }

    QString query(
        "SELECT r.number,r.floor,r.phone,rt.description,rt.cost\n"
        "FROM rooms AS r\n"
        "  INNER JOIN room_types AS rt\n"
        "  ON r.room_types_id = rt.id");

    qDebug() << "query:" << query;
    m_pimpl->rooms_model.setQuery(query);

    if (m_pimpl->rooms_model.lastError().isValid()) {
        qDebug("failed to get guests table (%s)",
               qPrintable(m_pimpl->rooms_model.lastError().text()));
        return nullptr;
    }

    QStringList headers = {tr("Номер комнаты"), tr("Этаж"), tr("Телефон"),
                           tr("Тип комнаты"), tr("Стоимость проживания")};

    for (int i{0}; i < m_pimpl->rooms_model.columnCount(); i++) {
        m_pimpl->rooms_model.setHeaderData(i, Qt::Horizontal, headers[i]);
    }

    return &m_pimpl->rooms_model;
}

QStringList HotelDB::getWeekdays()
{
    return getList("description", "weekday");
}

QStringList HotelDB::getFloors()
{
    return getList("floor", "floors");
}

int HotelDB::getFreeRoomsCount()
{
    if (m_pimpl->user_group >= Groups::EMPLOYEE) {
        return false;
    }

    QSqlQuery query(m_pimpl->db);

    if (!query.exec("SELECT COUNT(*) FROM rooms") || !query.next()) {
        qDebug("failed get room count (%s)", qPrintable(query.lastError().text()));
        return -1;
    }

    int room_count = query.value(0).toInt();

    if (!query.exec("SELECT COUNT(*) FROM guests_rooms") || !query.next()) {
        qDebug("failed get room count (%s)", qPrintable(query.lastError().text()));
        return -1;
    }

    int guest_count = query.value(0).toInt();

    qDebug() << "rooms count:" << room_count << "guests_count:" << guest_count;

    return room_count - guest_count;
}

bool HotelDB::getBill(int guest_id, hotel_bill_t& bill)
{
    if (m_pimpl->user_group >= Groups::EMPLOYEE) {
        return false;
    }

    QSqlQuery query;

    query.prepare("SELECT g.surname,g.firstname,g.patronym,r.number,g.arrival_date,"
                  "       g.duration,(g.duration * rt.cost) AS bill\n"
                  "FROM guests AS g\n"
                  "  INNER JOIN rooms AS r\n"
                  "  INNER JOIN guests_rooms AS gr\n"
                  "  ON g.id = gr.guests_id AND r.id = gr.rooms_id\n"
                  "  INNER JOIN room_types AS rt ON rt.id = r.room_types_id\n"
                  "WHERE g.id = ?");
    query.addBindValue(guest_id);

    if (!query.exec() || !query.next()) {
        qDebug("cannot get user data (%s)", qPrintable(query.lastError().text()));
        return false;
    }

    QStringList acronym;

    for (int i{0}; i < 3; i++) {
        acronym << query.value(i).toString();
    }

    bill.acronym = acronym.join(' ');
    bill.room = query.value(3).toInt();
    bill.date = query.value(4).toString();
    bill.duration = query.value(5).toInt();
    bill.bill = query.value(6).toInt();

    return true;
}

QStringList HotelDB::getList(QString column, QString table)
{
    QString query_str = QString("SELECT %1 FROM %2").arg(column).arg(table);
    QSqlQuery query(m_pimpl->db);

    if (!query.exec(query_str)) {
        qDebug("failed to get list: column(%s), table(%s) (%s)", qPrintable(column),
               qPrintable(table), qPrintable(query.lastError().text()));
        return {};
    }

    QStringList list;

    while (query.next()) {
        list << query.value(0).toString();
    }

    return list;
}

bool HotelDB::removeRecord(QString table, QString column, int column_value)
{
    QString query_str =
        QString("DELETE FROM %1 WHERE %2 = %3").arg(table).arg(column).arg(column_value);
    QSqlQuery query(m_pimpl->db);

    if (!query.exec(query_str)) {
        qDebug("failed to remove record id(%d) from table(%s) (%s)", column_value,
               qPrintable(table), qPrintable(query.lastError().text()));
        return false;
    }

    qDebug("id(%d) is removed from table(%s)", column_value, qPrintable(table));

    return true;
}

int HotelDB::getMaxID(QString table)
{
    QString query_str = QString("SELECT MAX(id) AS max_id FROM %1").arg(table);
    QSqlQuery query(m_pimpl->db);

    if (!query.exec(query_str) || !query.next()) {
        qDebug("cannot get max id from '%s' (%s)", qPrintable(table),
               qPrintable(query.lastError().text()));
        return -1;
    }

    return query.value(0).toInt();
}

bool HotelDB::validateUser(QString login, QString password)
{
    QSqlQuery query(m_pimpl->db);

    query.prepare("SELECT password,groups_id FROM users\n"
                  "WHERE name = ?");
    query.addBindValue(login);

    if (!query.exec()) {
        qDebug("cannot get user data (%s)", qPrintable(query.lastError().text()));
        return false;
    }

    if (!query.next()) {
        qDebug() << "cannot find" << login;
        return false;
    }

    QString password_hash = QCryptographicHash::hash(password.toUtf8(),
                                                     QCryptographicHash::Sha256).toHex();

    if (password_hash != query.value(0).toString()) {
        qDebug() << "wrong password";
        return false;
    }

    m_pimpl->user_group = query.value(1).toInt();

    return true;
}

} // namespace HotelDB
