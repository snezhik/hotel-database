#ifndef EMPLOYEESADDDIALOG_H
#define EMPLOYEESADDDIALOG_H

#include <QDialog>

namespace Ui {
class EmployeesDialog;
}

namespace HotelDB {
struct employees_filter_t;
}

class QAbstractItemModel;

class EmployeesAddDialog : public QDialog
{
    Q_OBJECT

public:
    EmployeesAddDialog(HotelDB::employees_filter_t* employee, QAbstractItemModel* floor,
                       QAbstractItemModel* weekdays, QWidget *parent = nullptr);
    ~EmployeesAddDialog();

private:
    Ui::EmployeesDialog *ui{nullptr};
    HotelDB::employees_filter_t* m_employee{nullptr};

public slots:
    void on_action_pb_clicked();
};

#endif // EMPLOYEESADDDIALOG_H
