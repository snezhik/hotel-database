-- room_types
INSERT INTO room_types (num_of_rooms, cost, description)
VALUES (1, 1500,"однокомнатная");
INSERT INTO room_types (num_of_rooms, cost, description)
VALUES (2, 2500,"двухкомнатная");
INSERT INTO room_types (num_of_rooms, cost, description)
VALUES (3, 3500,"трехкомнатная");

-- floors
INSERT INTO floors VALUES (1);
INSERT INTO floors VALUES (2);
INSERT INTO floors VALUES (3);

-- rooms
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (101, 1, 1, "812-01-01");
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (102, 1, 2, "812-01-02");
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (103, 1, 1, "812-01-03");
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (201, 2, 3, "812-02-01");
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (202, 2, 2, "812-02-02");
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (203, 2, 2, "812-02-03");
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (301, 3, 1, "812-03-01");
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (302, 3, 3, "812-03-02");
INSERT INTO rooms (number, floor, room_types_id, phone)
VALUES (303, 3, 3, "812-03-03");

-- guests
INSERT INTO guests (surname, firstname, patronym, passport, city, arrival_date, duration)
VALUES ("Петров", "Василий", "Иванович", "412471", "Хабаровск", "23-07-19", 5);
INSERT INTO guests (surname, firstname, patronym, passport, city, arrival_date, duration)
VALUES ("Аксенова", "Алиса", "Юрьевна", "314912", "Днепр", "22-07-19", 4);
INSERT INTO guests (surname, firstname, patronym, passport, city, arrival_date, duration)
VALUES ("Семенова", "Алевтина", "Петровна", "556985", "Минск", "20-05-19", 7);
INSERT INTO guests (surname, firstname, patronym, passport, city, arrival_date, duration)
VALUES ("Барановская", "Юлия", "Геннадьевна", "745631", "Москва", "15-06-19", 2);
INSERT INTO guests (surname, firstname, patronym, passport, city, arrival_date, duration)
VALUES ("Гордон", "Александр", "Александрович", "436987", "Москва", "15-06-19", 2);

-- guests_rooms
INSERT INTO guests_rooms (guests_id, rooms_id) VALUES (1, 1);
INSERT INTO guests_rooms (guests_id, rooms_id) VALUES (2, 3);
INSERT INTO guests_rooms (guests_id, rooms_id) VALUES (3, 8);
INSERT INTO guests_rooms (guests_id, rooms_id) VALUES (4, 5);
INSERT INTO guests_rooms (guests_id, rooms_id) VALUES (5, 7);

-- employees
INSERT INTO employees (surname, firstname, patronym)
VALUES ("Петров", "Иван", "Андреевич");
INSERT INTO employees (surname, firstname, patronym)
VALUES ("Алексеева", "Маруся", "Викторовна");
INSERT INTO employees (surname, firstname, patronym)
VALUES ("Илясов", "Потап", "Сергеевич");

-- weekday
INSERT INTO weekday (description) VALUES ("Понедельник");
INSERT INTO weekday (description) VALUES ("Вторник");
INSERT INTO weekday (description) VALUES ("Среда");
INSERT INTO weekday (description) VALUES ("Четверг");
INSERT INTO weekday (description) VALUES ("Пятница");
INSERT INTO weekday (description) VALUES ("Суббота");
INSERT INTO weekday (description) VALUES ("Воскресенье");

-- timetable
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (1, 1, 1);
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (2, 2, 1);
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (3, 3, 1);
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (1, 2, 3);
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (2, 3, 3);
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (3, 1, 3);
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (1, 3, 5);
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (2, 1, 5);
INSERT INTO timetable (floor, employees_id, weekday_id) VALUES (3, 2, 5);

-- groups
INSERT INTO groups (description) VALUES ("admin");
INSERT INTO groups (description) VALUES ("manager");
INSERT INTO groups (description) VALUES ("employee");

-- users
INSERT INTO users (name, password, groups_id)
VALUES ("admin", "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918", 1);
INSERT INTO users (name, password, groups_id)
VALUES ("manager", "6ee4a469cd4e91053847f5d3fcb61dbcc91e8f0ef10be7748da4c4a1ba382d17", 2);
INSERT INTO users (name, password, groups_id)
VALUES ("employee", "2fdc0177057d3a5c6c2c0821e01f4fa8d90f9a3bb7afd82b0db526af98d68de8", 3);
