#include <QRegExpValidator>

#include "hoteldb.h"

#include "guestssearchdialog.h"
#include "ui_guestsdialog.h"

GuestsSearchDialog::GuestsSearchDialog(HotelDB::guests_filter_t* guests_filter,
                                       QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::GuestsDialog)
    , m_guests_fiter(guests_filter)
{
    ui->setupUi(this);

    QRegExpValidator* only_digits = new QRegExpValidator(QRegExp("[0-9]*"), this);

    ui->duration_le->setValidator(only_digits);
    ui->room_number_le->setValidator(only_digits);
    ui->action_pb->setText("&Поиск");

    setWindowTitle("Поиск");
    setFixedSize(size());

    initializeUI();
}

GuestsSearchDialog::~GuestsSearchDialog()
{
    delete ui;
}

void GuestsSearchDialog::setLETextFromStr(QLineEdit* le, QString text,
                                          QString invalid_value)
{
    if (text != invalid_value) {
       le->setText(text);
    }
}

void GuestsSearchDialog::setLETextFromInt(QLineEdit* le, int number, int invalid_value)
{
    if (number != invalid_value) {
        le->setText(QString::number(number));
    }
}

template<typename ValueType>
void GuestsSearchDialog::setFilterValue(ValueType& filter, ValueType value,
                                        ValueType invalid_value)
{
    if (value != invalid_value) {
        filter = value;
    } else {
        filter = invalid_value;
    }
}

void GuestsSearchDialog::initializeUI()
{
    setLETextFromStr(ui->surname_le, m_guests_fiter->surname);
    setLETextFromStr(ui->firstname_le, m_guests_fiter->firstname);
    setLETextFromStr(ui->patronym_le, m_guests_fiter->patronym);
    setLETextFromStr(ui->passport_le, m_guests_fiter->passport);
    setLETextFromStr(ui->city_le, m_guests_fiter->city);
    setLETextFromStr(ui->arrival_date_le, m_guests_fiter->arrival_date);
    setLETextFromInt(ui->duration_le, m_guests_fiter->duration);
    setLETextFromInt(ui->room_number_le, m_guests_fiter->room_number);
}

void GuestsSearchDialog::on_action_pb_clicked()
{
    setFilterValue(m_guests_fiter->surname, ui->surname_le->text(), SQL_INVALID_TEXT);
    setFilterValue(m_guests_fiter->firstname, ui->firstname_le->text(), SQL_INVALID_TEXT);
    setFilterValue(m_guests_fiter->patronym, ui->patronym_le->text(), SQL_INVALID_TEXT);
    setFilterValue(m_guests_fiter->passport, ui->passport_le->text(), SQL_INVALID_TEXT);
    setFilterValue(m_guests_fiter->city, ui->city_le->text(), SQL_INVALID_TEXT);
    setFilterValue(m_guests_fiter->arrival_date, ui->arrival_date_le->text(),
                   SQL_INVALID_TEXT);

    bool ok{false};

    if (int value = ui->duration_le->text().toInt(&ok); ok) {
        setFilterValue(m_guests_fiter->duration, value, SQL_INVALID_INT);
    } else {
        m_guests_fiter->duration = SQL_INVALID_INT;
    }

    if (int value = ui->room_number_le->text().toInt(&ok); ok) {
        setFilterValue(m_guests_fiter->room_number, value, SQL_INVALID_INT);
    } else {
        m_guests_fiter->room_number = SQL_INVALID_INT;
    }

    accept();
}
