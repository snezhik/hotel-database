#include "findcleanerdialog.h"
#include "ui_findcleanerdialog.h"

FindCleanerDialog::FindCleanerDialog(QString guest, QAbstractItemModel* weekdays,
                                     SearchFunc search_func, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::FindCleanerDialog)
    , m_search_func(std::move(search_func))
{
    ui->setupUi(this);
    ui->guest_le->setText(guest);
    ui->weekdays_cb->setModel(weekdays);
}

FindCleanerDialog::~FindCleanerDialog()
{
    delete ui;
}

void FindCleanerDialog::on_search_pb_clicked()
{
    int weekday = ui->weekdays_cb->currentIndex();
    ui->cleaner_le->setText(m_search_func(weekday));
}
